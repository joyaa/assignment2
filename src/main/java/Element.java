import com.google.gson.annotations.SerializedName;

class Element {
	@SerializedName("owner_department")
	String ownerDepartment;
	
	@SerializedName("account")
	String account;
	
	@SerializedName("type")
	String type;
	
	public String getAccount() {
		return account;
	}
	
	public String getType() {
		return type;
	}
	
	@Override
    public String toString() {
        return "Element [owner_department=" + ownerDepartment + ", account=" + account
                + ", type=" + type + "]";
    }  
}