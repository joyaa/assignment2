import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;

public class EnglishEditorServiceImpl implements EditorService {

	Gson gson;
	ArrayList<Element> jsonElements;
		
	private String urlToJsonString(String urlString) throws IOException {
		URL url = null;
		BufferedReader reader = null;	
		url = new URL(urlString);
		reader = new BufferedReader(new InputStreamReader(url.openStream()));
		StringBuffer buffer = new StringBuffer();
		int read;
		char[] chars = new char[1024];
		
		while((read = reader.read(chars)) != -1){
			buffer.append(chars, 0, read);
		}
		
		if (reader != null){
			reader.close();
		}
		
		return buffer.toString();
	}
	
	private void parseJsonString(String json) {
		JsonParser parser = new JsonParser();
	    gson = new Gson();
	    
	    JsonArray Jarray = parser.parse(json).getAsJsonArray();

	    for(JsonElement obj : Jarray){
		   jsonElements.add(gson.fromJson(obj, Element.class));
	    }        
	}
	
	public void loadJson() throws IOException {
		//url to get json 
		final String urlString = "https://www.cs.utexas.edu/~devdatta/ej42-f7za.json";
		String json = urlToJsonString(urlString);
		
		//parsing the json-string array
		jsonElements = new ArrayList<Element>();
		parseJsonString(json);
	}

	public String getAccountCount(String param) throws IOException {
		loadJson();
		
        //process parameters
		int count = 0;
		String correctName = "";
		for (Element element : jsonElements) {
			if(param.equalsIgnoreCase(element.account) && !element.type.isEmpty()) {
				count++;
				if (correctName.isEmpty()) {
					correctName = element.account;
				}
			}
		}	
		if (correctName.isEmpty()) {
			correctName = param;
		}
		return gson.toJson(new JsonAccountStats(correctName, count));
	}
	
	public String getTypeCount(String param) throws IOException {
		loadJson();
		
        //process parameters
		int count = 0;
		String correctName = "";
		
		for (Element element : jsonElements) {
			if(param.equalsIgnoreCase(element.type)) {
				count++;
				if (correctName.isEmpty()) {
					correctName = element.type;
				}
			}
		}	
		if (correctName.isEmpty()) {
			correctName = param;
		}
		return gson.toJson(new JsonTypeStats(correctName, count));
	}
}
