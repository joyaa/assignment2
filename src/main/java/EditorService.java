import java.io.IOException;

public interface EditorService {
	
	public String getAccountCount(String param) throws IOException;

	public String getTypeCount(String param) throws IOException;

}
