import java.io.IOException;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class EmailController {

	private EditorService editorService;
	
	@ResponseBody
    @RequestMapping(value = "/")
    public String helloWorld()
    {
        return "Use: /austinsocialmedia?params";
    }
	
	@ResponseBody
	@RequestMapping(value = "/austinsocialmedia")
	public String getComposedEmail() {
		return "Specify Parameters";
	}	
	
	@ResponseBody
    @RequestMapping(value = "/austinsocialmedia", params = {"accountstats"}, method=RequestMethod.GET)
    public String getAccountStats(@RequestParam("accountstats") String param) throws IOException
    {
		return editorService.getAccountCount(param);
    }
	
	@ResponseBody
    @RequestMapping(value = "/austinsocialmedia", params = {"typestats"}, method=RequestMethod.GET)
    public String getTypeStats(@RequestParam("typestats") String param) throws IOException
    {
		return editorService.getTypeCount(param); 
    }
	
	public void setEditorService(EditorService mockEditor) {
		this.editorService = mockEditor;
	}
}
