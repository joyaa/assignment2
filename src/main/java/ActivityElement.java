import com.google.gson.annotations.SerializedName;

class ActivityElement extends UrlElement {
	@SerializedName("incoming_percentage")
	String incomingPercentage;
	
	@SerializedName("incoming_activity")
	String incomingActivity;
	
	@SerializedName("total_activity")
	String totalActivity;
	
	@SerializedName("outgoing_activity")
	String outgoingActivity;
	
}