import com.google.gson.annotations.SerializedName;

class UrlElement extends Element {
	@SerializedName("external_link")	
	Link url;
	
	public String getLink() {
		return url.getLink();
	}
	
	@Override
    public String toString() {
        return super.toString() + ", link="+url.getLink()+"]";
    }  
}