
public class JsonAccountStats {
    private String account;
    private String num_of_types;
    
    public JsonAccountStats(final String param, final int count) {
        account = param;
        num_of_types = Integer.toString(count);
    }
}
