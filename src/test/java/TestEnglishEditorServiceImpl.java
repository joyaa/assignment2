import java.io.IOException;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class TestEnglishEditorServiceImpl {

	EnglishEditorServiceImpl englishEditor = null;
	
	@Before
	public void setUp() {
		englishEditor = new EnglishEditorServiceImpl();
	}
	
	@Test
	public void testGetTypeCount() throws IOException {
		String output = englishEditor.getTypeCount("GOOGLE_PLUS");
		assertEquals("{\"type\":\"GOOGLE_PLUS\",\"num_of_accounts\":\"5\"}", output);
	}
	
	@Test
	public void testGetAccountCount() throws IOException {
		String output = englishEditor.getAccountCount("Ae");
		assertEquals("{\"account\":\"AE\",\"num_of_types\":\"6\"}", output);
	}
}
