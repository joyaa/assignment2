import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

import java.io.IOException;

public class TestEmailController {
	EmailController emailController = new EmailController();
	EditorService mockEditor = null;
	
	@Before
	public void setUp1() {		
		mockEditor = mock(EditorService.class);		
		emailController.setEditorService(mockEditor);
	}
		
	@Test
	public void testGetAccountStatsCorrectAction() throws IOException {
		when(mockEditor.getAccountCount("Ae")).thenReturn("{“account”:“AE”,“num_of_types”:“6”}");
		String output = emailController.getAccountStats("Ae");
		assertEquals("{“account”:“AE”,“num_of_types”:“6”}", output);
		verify(mockEditor).getAccountCount("Ae");
	}
	
	@Test
	public void testGetAccountStatsIncorrectAction() throws IOException {
		when(mockEditor.getAccountCount("AE123")).thenReturn("{“account”:“AE123”,“num_of_types”:“0”}");
		String output = emailController.getAccountStats("AE123");
		assertEquals("{“account”:“AE123”,“num_of_types”:“0”}", output);
		verify(mockEditor).getAccountCount("AE123");
	}
	
	@Test
	public void testGetTypeStatsCorrectAction() throws IOException {
		when(mockEditor.getTypeCount("GOOGLE_PLUS")).thenReturn("{“type”:“GOOGLE_PLUS”,“num_of_accounts”:“5”");
		String output = emailController.getTypeStats("GOOGLE_PLUS");
		assertEquals("{“type”:“GOOGLE_PLUS”,“num_of_accounts”:“5”", output);
		verify(mockEditor).getTypeCount("GOOGLE_PLUS");
	}

	@Test
	public void testGetTypeStatsInorrectAction() throws IOException {
		when(mockEditor.getTypeCount("INSTAGRAM123")).thenReturn("{“type”:“INSTAGRAM123”,“num_of_accounts”:“0”}");
		String output = emailController.getTypeStats("INSTAGRAM123");
		assertEquals("{“type”:“INSTAGRAM123”,“num_of_accounts”:“0”}", output);
		verify(mockEditor).getTypeCount("INSTAGRAM123");
	}
	
	@Test
	public void testGetTypeStatsInorrectAction2() throws IOException {
		when(mockEditor.getTypeCount("")).thenReturn("{“type”:“”,“num_of_accounts”:“0”}");
		String output = emailController.getTypeStats("");
		assertEquals("{“type”:“”,“num_of_accounts”:“0”}", output);
		verify(mockEditor).getTypeCount("");
	}
	
}
